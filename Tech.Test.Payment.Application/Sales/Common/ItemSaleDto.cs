﻿namespace Tech.Test.Payment.Application.Sales.Common;

public record ItemSaleDto(string Name, decimal Price,int Quantity);
