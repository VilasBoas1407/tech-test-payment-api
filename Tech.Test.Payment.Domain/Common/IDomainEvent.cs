﻿
using MediatR;

namespace Tech.Test.Payment.Domain.Common;

public interface IDomainEvent : INotification
{ }
