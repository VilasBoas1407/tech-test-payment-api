﻿namespace Tech.Test.Payment.Contracts.Sales;

public record CreateSaleResponse(Guid Id,string CustomerName, int TotalItens);
